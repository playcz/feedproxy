<?php
//  Cache module
// (c) Ladislav Soukup 2010, www.ladasoukup.cz
// with WinCache support (IIS module for memory caching)
class cache {
	var $version = "1.50";
	var $killcache = false;
	
	function __construct() {
		if (_CACHE_KILL === true) {
			$this->killcache = true;
		}
	}
	
	function getVer() {
		return($this->version);
	}
	
	function getCacheFilepath($cacheID, $retAsArray = false) {
		global $core;
		$ret = '';
		$retArray = array();
		
		$retArray['cacheID'] = $cacheID;
		
		$bad = array("../", "./", "/", "<", ">", "'", '"', '&', '$', '#', '?', '*');
		
		$cdir = '';
		$tmp = explode('~', $cacheID);
		if (sizeof($tmp) > 1) {
			$cdir = $tmp[0] . '/';
			$tmp = _CACHE_PATH . $cdir;
			// echo $tmp;
			if (!is_dir($tmp)) mkdir($tmp, 0777);
			
			$retArray['path'] = $tmp;
		}
		
		$retArray['basepath'] = _CACHE_PATH;
		$retArray['cdir'] = $cdir;
		$retArray['file'] = str_replace($bad, '_', $cacheID);
		$retArray['ext'] = '.tmp';
		
		$ret = _CACHE_PATH . $cdir . $retArray['file'] . $retArray['ext'];
		
		if ($retAsArray === true) $ret = $retArray;
		return($ret);
	}
	
	function setCache($cacheID, $data, $cache_lifetime = _CACHE_LIFE) {
		// if ($this->killcache === true) return(false);
		
		if ($cache_lifetime > 0) {
			$cache_validto = time() + $cache_lifetime;
			$data_s = serialize(array('validto' => $cache_validto, 'data' => $data));
			
			if ( extension_loaded( 'wincache' ) ) {
				wincache_ucache_set($cacheID, $data_s, $cache_lifetime);
			} else {
				$cache_file = $this->getCacheFilepath($cacheID);
				if (file_exists($cache_file)) unlink($cache_file);
				@file_put_contents($cache_file, $data_s);
			}
		}
	}
	
	function getCache($cacheID, $cache_lifetime = _CACHE_LIFE) {
		if ($this->killcache === true) return(false);
		
		$ret = false;
		if ($cache_lifetime > 0) {
			if ( extension_loaded( 'wincache' ) ) {
				$is_cached = false;
				$ret = wincache_ucache_get($cacheID, $is_cached);
				if ($is_cached === true) {
					$ret = unserialize($ret);
					$ret = $ret['data'];
				} else {
					$ret = false;
				}
			} else {
				$cache_file = $this->getCacheFilepath($cacheID);
				if (file_exists($cache_file)) {
					$data = unserialize(file_get_contents($cache_file));
					if ($data['validto'] > time()) {
						$ret = $data['data'];
					} else {
						$ret = false;
						unlink($cache_file);
					}
				}
			}
		}
		return($ret);
	}
}