<?php
class helpers {
	
	function parseRSSitem($item) {
		$ret = '';
		$tmp_img = '';
		
		$ret['title'] = $item['title'];
		$ret['description'] = trim(strip_tags($item['description']));
		$ret['link'] = $item['link'];
		$ret['pubDate'] = $item['pubDate'];
		$ret['pubDate_TS'] = strtotime($item['pubDate']);
		
		$tmp_img = $this->catch_that_image($item['description']); // parse out first image from descrition
		if (empty($tmp_img)) {
			$tmp_img = $this->catch_that_image_url($item['link']); // parse out first image from web page of post
		}
		
		$ret['image'] = $tmp_img;
		
		return ($ret);
	}
	
	function catch_that_image($html) {
		$first_img = '';
		
		$DOM = new simple_html_dom();
		$DOM->load($html);
		foreach($DOM->find('img') as $element) {
			if (empty($first_img)){
				if ($element->width != 1) {
					$first_img = $element->src;
				}
			}
		}
		$DOM->clear(); 
		unset($DOM);
		
		return($first_img);
	}
	
	function catch_that_image_url($link) {
		$first_img = ''; $selector = '';
		
		$DOM = file_get_html($link, 'html');
		
		if (empty($first_img)) {
			$selector = 'meta[property="og:image"]';
			foreach($DOM->find($selector) as $element) {
				if (empty($first_img)) {
					$first_img = $element->content;
				}
			}
		}
		
		if (empty($first_img)) {
			$selector = '#content img';
			foreach($DOM->find($selector) as $element) {
				if (empty($first_img)){
					if ($element->width != 1) {
						$first_img = $element->src;
					}
				}
			}
		}
		
		$DOM->clear(); 
		unset($DOM);
		return($first_img);
	}
	
	function object2array($object) {
		$return = NULL;
		if(is_array($object)) {
			foreach($object as $key => $value)
				$return[$key] = $this->object2array($value);
			} else {
				$var = get_object_vars($object);
				if($var) {
					foreach($var as $key => $value)
						$return[$key] = $this->object2array($value);
				} else {
					return strval($object); // strval and everything is fine
				}
			}
		return $return;
	}
	
	function array2xml($array) {
		$ret = '';
		foreach ($array as $key=>$val) {
			if (is_int($key)) $key = 'loop';
			$r1 = array('(', ')', '-', '>', '<');
			$r2 = array('', '', '', '', '');
			$key = str_replace($r1, $r2, $key);
			
			if (is_array($val)) {
				$ret .= '<'.$key.'>' . $this->array2xml($val) . '</'.$key.'>' . "\n";
			} else {
				$ret .= '<'.$key.'><![CDATA[' . $val . ']]></'.$key.'>' . "\n";
			}
		}
		return($ret);
	}
	
	function A($array, $title = 'array') {
		echo '<h3>' . $title . '</h3>';
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
	
	function is_nil ($value) {
		return !$value && $value !== 0 && $value !== '0';
	}
	
	function def ($value, $defaultValue) {
		return $this->is_nil ($value) ? $defaultValue : $value;
	}
	
}
?>