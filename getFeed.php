<?php
/**
 * FeedProxy
 * https://github.com/soukupl/FeedProxy
 *
 * Copyright 2011 Ladislav Soukup
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

define('_CACHE_PATH', './000cache/'); /* cache directory */
define('_CACHE_KILL', false); /* kill cache - eg. for testing. cache is created, but never used */
define('_CACHE_LIFE', 3600 + rand(900, 1800)); /* cache lifetime in seconds (inc random) */

require_once('./lib/helpers.php'); $helpers = new helpers(); /* load helpers lib */
require_once('./lib/security.php'); $sec = new security(); /* load security lib */
require_once('./lib/cache.php'); $cache = new cache(); /* load caching lib */
require_once('./lib/rss_php.php'); $rss = new rss_php; /* RSS lib */
require_once('./lib/simple_html_dom.php'); /* HTML DOM parser */

$feed = urldecode($sec->get('feed'));
$count = $helpers->def($sec->get('count'), 10);
$format = $helpers->def($sec->get('format'), 'jsonp');
$callback = $sec->get('callback');
$ret = array(); /* output data array */

/* try to load from cache */
$_cacheID = 'FeedProxy-' . $count . '-' . sha1( $feed );
$_cache_lifetime = 3600; /* lifetime of cache */
$ret = $cache->getCache($_cacheID, _CACHE_LIFE);
// $ret = false;  /* DEBUG ONLY */

if (empty($ret)) {
	/* do the job */
	
	$rss->load($feed); // load feed
	$feed = $rss->getRSS(); // parse RSS
	
	$feed_img = $feed['rss']['channel']['image']['url'];
	$feed_title = $feed['rss']['channel']['title'];
	
	$ret['data']['meta']['title'] = $feed_title;
	$ret['data']['meta']['image'] = $feed_img;
	$ret['data']['meta']['count'] = $count;
	
	for ($loop=0; $loop<$count; $loop++) {
		$ret['data']['item'][] = $helpers->parseRSSitem($feed['rss']['channel']['item:'.$loop]);
	}
	// $ret['data']['meta']['count'] = $loop;
	
	$cache->setCache($_cacheID, $ret, _CACHE_LIFE); /* store to cache */
}



// DISPLAY CONTENT
$_OUT = '';

header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + _CACHE_LIFE)); /* set expire header */
switch ($format) {
	case 'xml':
		header('Content-type: text/xml; charset=utf-8');
		$_OUT .= '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>' . "\n";
		$_OUT .= '<xml>';
		$_OUT .= $helpers->array2xml($ret);
		$_OUT .= '</xml>';
		break;
	case 'json':
		header('Content-type: text/json; charset=utf-8');
		$_OUT .= json_encode($ret);
		break;
	case 'jsonp':
		header('Content-type: text/javascript; charset=utf-8');
		$_OUT .= $callback.'(';
		$_OUT .= json_encode($ret);
		$_OUT .= ');';
		break;
	case 'debug':
		header('Content-type: text/html; charset=utf-8');
		ob_start();
		$helpers->A($ret);
		$_OUT = ob_get_clean();
		break;
}
echo $_OUT;
?>