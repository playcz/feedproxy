FeedProxy
=========

FeedProxy is simple PHP script to proxy RSS feeds with added images in form of JSON, JSONP or XML data source.
FeedProxy is intended to be used as backend proxy for JavaScript and AJAX front-end.

FeedProxy can read RSS source and add image to each item. All returned data are clean without HTML.
FeedProxy tries to find image in the description of each item (if description is formated with HTML).
If no image found, FeedProxy will try to find first image on the linked article (currently, using selecotr "#content img"). 


USAGE
-----
It's simple. Just point your browser or AJAX request (or app request) to:
http://-YOUR-HOST-WITH-PATH-/getFeed.php?format=xml&count=5&feed=http://feeds.feedburner.com/playcz

There are several parameters you can use:

format - type of response (xml, json, jsonp, debug)
count - max count of items from RSS (default is 10)
feed - URL of the feed. This should be URL encoded
callback - jsonp callback

Sample jQuery AJAX call
-----------------------
	var feedURL = 'http://feeds.feedburner.com/playcz';
	var url = 'http://feedproxy.example.com/getFeed.php/?format=xml&count=5&feed=' + encodeURIComponent(feedURL) + '&callback=?';
	$.getJSON(url, function(data) {
		$.each(data.data.item, function (i, entry) {
			// entry.link
			// entry.title
			// entry.description
			// entry.image
			// entry.pubDate
		});
	});


INSTALL
-------
FeedProxy is standalone proxy. Just upload to your web server to as sub-directory or set it as sub domain (ex.: feedproxy.example.com)
PHP 5.x is required, PHP5.2 is tested. FeedProxy is using fopen URL wrappers to download RSS feeds.

FeedProxy is using it's own caching lib (./lib/cache.php) - you can use your own if you like.
Cache lib supports file mode or WinCache by Microsoft (for IIS server).
Mode is switched automatically. All cached data are stored as files under the '000cache' directory (please make it writable for the script).
Cache can be configured in getFeed.php script (first few lines).

please note: you have to create top level cache directory manually and make it writable for PHP (default is '000cache' in the script root). I'm working on updated cache lib to support more memory options (memcached, etc…) and better file / directory handling.

AUTHOR
------

**Ladislav Soukup**

+ http://git.play.cz
+ http://git.ladasoukup.cz
+ http://www.ladasoukup.cz


USING following libs
--------------------
	Simple HTML DOM - https://github.com/jjanyan/Simple-HTML-DOM/
	RSS PHP - http://rssphp.net/
	Security Class - collected security classes over the internet


Copyright and License
---------------------

Copyright 2011 Ladislav Soukup

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.